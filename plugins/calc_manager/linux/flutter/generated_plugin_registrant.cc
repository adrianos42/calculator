//
//  Generated file. Do not edit.
//

#include "generated_plugin_registrant.h"

#include <calc_manager/calc_manager_plugin.h>

void fl_register_plugins(FlPluginRegistry* registry) {
  g_autoptr(FlPluginRegistrar) calc_manager_registrar =
      fl_plugin_registry_get_registrar_for_plugin(registry, "CalcManagerPlugin");
  calc_manager_plugin_register_with_registrar(calc_manager_registrar);
}
