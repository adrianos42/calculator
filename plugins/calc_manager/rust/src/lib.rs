use std::ffi::{CStr, CString};
use std::os::raw::c_char;

mod engine;

pub struct CalculatorManager {
    for_you: u64,
    display: Option<Progress>,
}

impl CalculatorManager {
    fn new() -> Self {
        CalculatorManager {
            for_you: 42,
            display: None,
        }
    }

    fn get_string(&self) -> String {
        self.for_you.to_string()
    }
}

#[no_mangle]
pub extern "C" fn rust_greeting(to: *const c_char) -> *mut c_char {
    let c_str = unsafe { CStr::from_ptr(to) };
    let recipient = match c_str.to_str() {
        Err(_) => "there",
        Ok(string) => string,
    };

    CString::new("Hello ".to_owned() + recipient)
        .unwrap()
        .into_raw()
}

#[no_mangle]
pub extern "C" fn rust_cstr_free(s: *mut c_char) {
    unsafe {
        if s.is_null() {
            return;
        }
        CString::from_raw(s)
    };
}

#[no_mangle]
pub extern "C" fn programmer_calculator_new() -> *mut CalculatorManager {
    let calc_man = CalculatorManager::new();
    Box::into_raw(Box::new(calc_man))
}

#[no_mangle]
pub extern "C" fn programmer_add_callback(calc_man: &mut CalculatorManager, display: Progress) {
    calc_man.display = Some(display);    
}

#[no_mangle]
pub extern "C" fn programmer_calculator_set_command(
    calc_man: &CalculatorManager,
    command: i64,
) -> *mut c_char {
    let to_print = CString::new("rerererere").unwrap();
    if let Some(progress) = calc_man.display {
        progress(to_print.as_ptr());
    }

    CString::new(calc_man.get_string() + " " + command.to_string().as_str())
        .unwrap()
        .into_raw()
}

type Progress = extern "C" fn(s: *const c_char);
