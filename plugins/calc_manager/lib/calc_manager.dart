import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:isolate';

import 'package:ffi/ffi.dart';
import 'src/ffi/lib_utils.dart';

class ProgrammerCalculator extends Struct {}

enum ProgrammerCalculatorMethods {
  sendCommand,
}

enum ProgrammerCalculatorCallbacks {
  display,
}

typedef Progress = Void Function(Pointer<Utf8>);

typedef ProgrammerCalculatorNewFunc = Pointer<ProgrammerCalculator> Function();

typedef ProgrammerCalculatorSendCommandNative = Pointer<Utf8> Function(
    Pointer<ProgrammerCalculator>, Int64);
typedef ProgrammerCalculatorSendCommandFunc = Pointer<Utf8> Function(
    Pointer<ProgrammerCalculator>, int);

typedef ProgrammerCalculatorRegisterCallbackNative = Void Function(
    Pointer<ProgrammerCalculator>, Pointer<NativeFunction<Progress>>);
typedef ProgrammerCalculatorRegisterCallbackFunc = void Function(
    Pointer<ProgrammerCalculator>, Pointer<NativeFunction<Progress>>);

class CalcManager {
  static Isolate _isolate;

  // Only used by main.
  static SendPort _toIsolatePort;

  // Only used by the isolate.
  static SendPort _toMainPort;

  static Pointer<ProgrammerCalculator> _programmerCalculator;
  static void Function(String) _displayCallback;

  static Future<SendPort> _setIsolate() async {
    final completer = Completer<SendPort>();
    final isolateToMain = ReceivePort();

    isolateToMain.listen((data) {
      if (data is List<dynamic>) {
        switch (data[0] as ProgrammerCalculatorCallbacks) {
          case ProgrammerCalculatorCallbacks.display:
            _displayCallback?.call(data[1]);
        }
      } else if (data is SendPort) {
        completer.complete(data);
      }
    });

    _isolate = await Isolate.spawn(_runIsolate, isolateToMain.sendPort);
    return completer.future;
  }

  static void progress(Pointer<Utf8> value) {
    _toMainPort
        .send([ProgrammerCalculatorCallbacks.display, Utf8.fromUtf8(value)]);
  }

  static void _runIsolate(SendPort sendPort) {
    _programmerCalculator = _programmerCalculatorNew();

    _programmerCalculatorAddCallback(
        _programmerCalculator, Pointer.fromFunction<Progress>(progress));

    final receivePort = ReceivePort();

    sendPort.send(receivePort.sendPort);
    _toMainPort = sendPort;
    _isolate = Isolate.current;

    receivePort.listen((data) {
      assert(data is List<dynamic>);
      switch (data[0] as ProgrammerCalculatorMethods) {
        case ProgrammerCalculatorMethods.sendCommand:
          var value = _programmerCalculatorSendCommand(
            _programmerCalculator,
            data[1],
          );
          final valueStr = Utf8.fromUtf8(value);
          print(valueStr);
          break;
      }
    });
  }

  static final DynamicLibrary _calcManagerLib = openLibrary('calc_manager');

  static final ProgrammerCalculatorNewFunc _programmerCalculatorNew =
      _calcManagerLib.lookupFunction<ProgrammerCalculatorNewFunc,
          ProgrammerCalculatorNewFunc>('programmer_calculator_new');

  static final ProgrammerCalculatorSendCommandFunc
      _programmerCalculatorSendCommand = _calcManagerLib.lookupFunction<
              ProgrammerCalculatorSendCommandNative,
              ProgrammerCalculatorSendCommandFunc>(
          'programmer_calculator_set_command');

  static final ProgrammerCalculatorRegisterCallbackFunc
      _programmerCalculatorAddCallback = _calcManagerLib.lookupFunction<
              ProgrammerCalculatorRegisterCallbackNative,
              ProgrammerCalculatorRegisterCallbackFunc>(
          'programmer_add_callback');

  static Future<void> sendCommand(int command) async {
    if (_toIsolatePort == null) {
      _toIsolatePort = await _setIsolate();
    }

    _toIsolatePort.send([
      ProgrammerCalculatorMethods.sendCommand,
      command,
    ]);
  }

  static Future<void> registerDisplayCallback<F>(F callback) async {
    if (_toIsolatePort == null) {
      _toIsolatePort = await _setIsolate();
    }

    _displayCallback = callback as void Function(String);
  }
}
