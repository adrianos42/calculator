import 'dart:ffi';
import 'dart:io' show Platform;


DynamicLibrary openLibrary(String name) {
  name = 'lib' + name;
  return Platform.isAndroid || Platform.isLinux
      ? DynamicLibrary.open(name + '.so')
      : Platform.isWindows
          ? DynamicLibrary.open(name + '.dll')
          : DynamicLibrary.process();
}
